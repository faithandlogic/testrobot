*** Settings ***
Library  Collections
Library  RequestsLibrary

*** Test Cases ***
Post Requests
    [Tags]  post
    Create Session  chikka  https://post.chikka.com/smsapi
    ${data}=  Create Dictionary  message_type=SEND  mobile_number=639499235814  shortcode=29290036982  message_id=fASTER     message=MAY THE FORCE BE WITH YOU    client_id=3ec593d7f48c1e2ae995b55d6023951fa479990d458b9946d6e7d7145c15139f      secret_key=1d092430f98f45a4832aaefb013bf93615fac6083fc597ea5ea8ab2fe67f0bb7
    ${headers}=  Create Dictionary  Content-Type=application/x-www-form-urlencoded
    ${resp}=  Post Request  chikka  /request  data=${data}  headers=${headers}
    Should be Equal As Strings      ${resp.status_code}      200
    Log     ${resp.content}

